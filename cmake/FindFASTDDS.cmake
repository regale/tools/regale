#[=======================================================================[.rst:
FindFASTDDS
===========

Find FASTDDS library

This module finds includes' and libraries' folder, of the installed FASTDDS library.

Input Variables
---------------

The following variables may be set to influence this module's behavior:

``FASTDDS_ROOT``
  The path to the installation folder of FastDDS library

Result Variables
----------------

This module will set the following variables in your project:

``FASTDDS_FOUND``
  FastDDS is found
``FASTDDS_LIBRARIES``
  the libraries needed to use FastDDS.
``FASTDDS_INCLUDE_DIRS``
  where to find headers for FastDDS

#]=======================================================================]

find_library(
    FASTDDS_LIBRARIES
    NAMES "fastrtps"
    HINTS ${FASTDDS_ROOT}
    PATH_SUFFIXES "lib" "lib64")

find_path(
    FASTDDS_INCLUDE_DIRS
    NAMES "fastrtps_all.h"
    HINTS ${FASTDDS_ROOT}
    PATH_SUFFIXES "include/fastrtps")

get_filename_component(FASTDDS_INCLUDE_DIRS
                       ${FASTDDS_INCLUDE_DIRS}
                       DIRECTORY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FASTDDS
                                  REQUIRED_VARS FASTDDS_LIBRARIES FASTDDS_INCLUDE_DIRS
                                  REASON_FAILURE_MESSAGE "Cannot find FastDDS, maybe try defining the variable -DFASTDDS_ROOT as the path to its installation.")

if(FASTDDS_FOUND)
    add_library(regale_fastdds
                INTERFACE)
    target_link_libraries(regale_fastdds
                          INTERFACE
                          ${FASTDDS_LIBRARIES})
    target_include_directories(regale_fastdds
                               INTERFACE
                               ${FASTDDS_INCLUDE_DIRS})
endif()
