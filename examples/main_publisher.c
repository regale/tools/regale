#include "regale.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void help_function(char* exe_name) {
    printf("Usage: %s <file_types.xml> <file_profiles.xml> <data_type> <transport_type>\n",
           exe_name);
    printf("<file_types.xml> is the path to the regale file containing the data types available\n");
    printf("<file_profiles.xml> is the path to the regale file containing the profiles available\n");
    printf("<data_type> is the chosen data type, selected from the ones contained in <file_types.xml>\n");
    printf("<transport_type> is the chosen transport type, selected from the ones contained in <profiles_types.xml>\n");
}

int main(int argc, char** argv) {
    char* types_file = argv[1];
    char* profiles_file = argv[2];
    char* topic = argv[3];
    char* transport = argv[4];
    uint32_t freqs[] = {3100000, 1000000};
    uint32_t freqs2[] = {2800000, 1500000};
    uint32_t freqs3[] = {2500000, 2000000};
    uint32_t pows[] = {1000, 2000, 3000};
    uint32_t pows2[] = {4000, 5000, 6000};
    uint32_t pows3[] = {7000, 8000, 9000};
    uint32_t* values = NULL;
    uint32_t* values2 = NULL;
    uint32_t* values3 = NULL;

    if (!strcmp(topic,
                "struct_freq")) {
        values = freqs;
        values2 = freqs2;
        values3 = freqs3;
    }
    else if (!strcmp(topic,
                     "struct_power")) {
        values = pows;
        values2 = pows2;
        values3 = pows3;
    }

    if (argc != 5) {
        help_function(argv[0]);

        return 1;
    }

    printf("Starting publisher.\n");

    RegalePublisher* pub = Regale_create_publisher(types_file   ,
                                                   profiles_file,
                                                   transport    ,
                                                   topic);

    Regale_publish(pub,
                   values);
    sleep(1);

    Regale_publish(pub,
                   values2);
    sleep(1);

    Regale_publish(pub,
                   values3);
    sleep(1);

    Regale_delete((RegaleObject*)pub);

    printf("Ended publisher.\n");

    return 0;
}

