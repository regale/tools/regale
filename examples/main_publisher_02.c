#include "regale.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void help_function(char* exe_name) {
    printf("Usage: %s <file_types.xml> <file_profiles.xml> <transport_type>\n",
           exe_name);
    printf("<file_types.xml> is the path to the regale file containing the data types available\n");
    printf("<file_profiles.xml> is the path to the regale file containing the profiles available\n");
    printf("<transport_type> is the chosen transport type, selected from the ones contained in <profiles_types.xml>\n");
}

int main(int argc, char** argv) {
    char* types_file = argv[1];
    char* profiles_file = argv[2];
    char* transport = argv[3];
    char topic[] = "struct_freq";
    char topic_02[] = "struct_power";
    uint32_t freqs[] = {3100000, 1000000};
    uint32_t pows[] = {1000, 2000, 3000};
    uint32_t* freq_values = NULL;
    uint32_t* pow_values = NULL;

    freq_values = freqs;
    pow_values = pows;

    if (argc != 4) {
        help_function(argv[0]);

        return 1;
    }

    printf("Starting publishers.\n");

    RegalePublisher* pub = Regale_create_publisher(types_file   ,
                                                   profiles_file,
                                                   transport    ,
                                                   topic);
    RegalePublisher* pub_02 = Regale_create_publisher(types_file   ,
                                                      profiles_file,
                                                      transport    ,
                                                      topic_02);

    Regale_publish(pub,
                   freq_values);
    sleep(1);
    Regale_publish(pub_02,
                   pow_values);

    Regale_delete((RegaleObject*)pub);
    Regale_delete((RegaleObject*)pub_02);

    printf("Ended publishers.\n");

    return 0;
}

