#include "regale.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>

void help_function(char* exe_name) {
    printf("Usage: %s <file_types.xml> <file_profiles.xml> <transport_type>\n",
           exe_name);
    printf("<file_types.xml> is the path to the regale file containing the data types available\n");
    printf("<file_profiles.xml> is the path to the regale file containing the profiles available\n");
    printf("<transport_type> is the chosen transport type, selected from the ones contained in <profiles_types.xml>\n");
}

void print_subscribed_freq_values(void* values) {
    printf("Received message with values %ld %ld\n",
           ((uint32_t*)values)[0]                  ,
           ((uint32_t*)values)[1]);
}

void print_subscribed_power_values(void* values) {
    printf("Received message with values %ld %ld %ld\n",
           ((uint32_t*)values)[0]                      ,
           ((uint32_t*)values)[1]                      ,
           ((uint32_t*)values)[2]);
}

int main(int argc, char** argv) {
    char* types_file = argv[1];
    char* profiles_file = argv[2];
    char* transport = argv[3];
    char topic[] = "struct_freq";
    char topic_02[] = "struct_power";
    uint64_t i;
    uint64_t j = 0;
    void (*ptr_func)(void*);
    void (*ptr_func_02)(void*);

    ptr_func = print_subscribed_freq_values;
    ptr_func_02 = print_subscribed_power_values;

    if (argc != 4) {
        help_function(argv[0]);

        return 1;
    }

    printf("Starting subscribers.\n");

    RegaleSubscriber* sub = Regale_create_subscriber(types_file   ,
                                                     profiles_file,
                                                     transport    ,
                                                     topic        ,
                                                     ptr_func);
    RegaleSubscriber* sub_02 = Regale_create_subscriber(types_file   ,
                                                        profiles_file,
                                                        transport    ,
                                                        topic_02     ,
                                                        ptr_func_02);

    for (i = 0; i < 2000000000; i++) {
        j += i;
    }
    printf("j = %ld\n", j);

    Regale_delete((RegaleObject*)sub);
    Regale_delete((RegaleObject*)sub_02);

    printf("Ended subscribers.\n");

    return 0;
}

