#include "regale.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>

void help_function(char* exe_name) {
    printf("Usage: %s <file_types.xml> <file_profiles.xml> <data_type> <transport_type>\n",
           exe_name);
    printf("<file_types.xml> is the path to the regale file containing the data types available\n");
    printf("<file_profiles.xml> is the path to the regale file containing the profiles available\n");
    printf("<data_type> is the chosen data type, selected from the ones contained in <file_types.xml>\n");
    printf("<transport_type> is the chosen transport type, selected from the ones contained in <profiles_types.xml>\n");
}

void print_subscribed_freq_values(void* values) {
    printf("Received message with values %ld %ld\n",
           ((uint32_t*)values)[0]                  ,
           ((uint32_t*)values)[1]);
}

void print_subscribed_power_values(void* values) {
    printf("Received message with values %ld %ld %ld\n",
           ((uint32_t*)values)[0]                      ,
           ((uint32_t*)values)[1]                      ,
           ((uint32_t*)values)[2]);
}

int main(int argc, char** argv) {
    char* types_file = argv[1];
    char* profiles_file = argv[2];
    char* topic = argv[3];
    char* transport = argv[4];
    uint64_t i;
    uint64_t j = 0;
    void (*ptr_func)(void*);

    if (!strcmp(topic,
                "struct_freq"))
        ptr_func = print_subscribed_freq_values;
    if (!strcmp(topic,
                "struct_power"))
        ptr_func = print_subscribed_power_values;

    if (argc != 5) {
        help_function(argv[0]);

        return 1;
    }

    printf("Starting subscriber.\n");

    RegaleSubscriber* sub = Regale_create_subscriber(types_file   ,
                                                     profiles_file,
                                                     transport    ,
                                                     topic        ,
                                                     ptr_func);

    for (i = 0; i < 2000000000; i++) {
        j += i;
    }
    printf("j = %ld\n", j);

    Regale_delete((RegaleObject*)sub);

    printf("Ended subscriber.\n");

    return 0;
}

