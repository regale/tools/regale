#ifndef __REGALE_H
#define __REGALE_H
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif
    typedef struct RegaleObject RegaleObject;
    typedef struct RegaleSubscriber RegaleSubscriber;
    typedef struct RegalePublisher RegalePublisher;

    RegalePublisher* Regale_create_publisher(char* types_file   ,
                                             char* profiles_file,
                                             char* transport    ,
                                             char* topic);
    RegaleSubscriber* Regale_create_subscriber(char* types_file   ,
                                               char* profiles_file,
                                               char* transport    ,
                                               char* topic        ,
                                               void (*ptr_func)(void*));

    bool Regale_publish(RegalePublisher* p,
                        uint32_t* values);

    void Regale_delete(RegaleObject* reg_obj);
#ifdef __cplusplus
}
#endif
#endif
