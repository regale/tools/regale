#include "RegaleObject.h"
#include <string>

using namespace eprosima::fastdds::dds;

std::once_flag RegaleObject::flag_once_;

RegaleObject::RegaleObject(char* types_file   ,
                           char* profiles_file,
                           char* transport    ,
                           char* topic        ,
                           void (*ptr_func)(void*)) {
    ptr_func_ = ptr_func;
    // Getting usage of C++ strings from a C array of chars.
    std::string types_file_str(types_file);
    std::string profiles_file_str(profiles_file);
    // Getting the name of the file containing the type of our data. See 
    // 1) https://stackoverflow.com/questions/8520560/get-a-file-name-from-a-path
    // and
    // 2) https://cplusplus.com/reference/string/string/find_last_of/
    //xml_file_str = xml_file_str.substr(xml_file_str.find_last_of("/") + 1);
    //file_type_ = xml_file_str.substr(0,
    //                                 xml_file_str.find_last_of("."));
    // Getting usage of C++ strings from a C array of chars.
    std::string topic_str(topic);
    std::string transport_str(transport);
    file_type_.assign(topic_str);

    DomainParticipantQos participantQos;
    std::string participant_qos_name = "Participant_" + topic_str;
    participantQos.name(participant_qos_name);
    // TODO: manage possible exceptions from this line until \"topic_\".
    // See: https://stackoverflow.com/questions/8412630/how-to-execute-a-piece-of-code-only-once
    std::call_once(RegaleObject::flag_once_,
                   [&types_file_str,
                    &profiles_file_str]() {
                       DomainParticipantFactory::get_instance()->load_XML_profiles_file(types_file_str);
                       DomainParticipantFactory::get_instance()->load_XML_profiles_file(profiles_file_str);
                    });
    xml_type_ = eprosima::fastrtps::xmlparser::XMLProfileManager::getDynamicTypeByName(file_type_)->build();
    transport_ = eprosima::fastrtps::xmlparser::XMLProfileManager::getTransportById(transport_str);
    participantQos.transport().user_transports.push_back(transport_);
    // No usage of the default transport.
    participantQos.transport().use_builtin_transports = false;
    if (!(transport_str.compare("tcpv4_server_transport"))) {
        participant_ = DomainParticipantFactory::get_instance()->create_participant_with_profile(0,
                                                                                                 "tcp_server_participant");
    } else if (!(transport_str.compare("tcpv4_client_transport"))) {
        participant_ = DomainParticipantFactory::get_instance()->create_participant_with_profile(0,
                                                                                                 "tcp_client_participant");
    } else {
        participant_ = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);
    }
    TypeSupport xml_type_support(new eprosima::fastrtps::types::DynamicPubSubType(xml_type_));
    xml_type_support.register_type(participant_);
    xml_data_ = (eprosima::fastrtps::types::DynamicDataFactory::get_instance()->create_data(xml_type_));

    topic_ = participant_->create_topic(topic_str ,
                                        file_type_,
                                        TOPIC_QOS_DEFAULT);

    // Notihing has been added for the two smart pointers \"xml_data_\" and \"xml_type_\", because
    // smart pointers should be \"nullptr\" by default (see https://cplusplus.com/forum/beginner/169675/).
    // Moreover, for the private string \"file_type_\", it is not passed to it a \"nullptr\" value, because the
    // behaviour could be undefined. See
    // https://stackoverflow.com/questions/10771864/assign-a-nullptr-to-a-stdstring-is-safe.
}

RegaleObject::~RegaleObject() {
    if (topic_ != nullptr)
        participant_->delete_topic(topic_);
    
    DomainParticipantFactory::get_instance()->delete_participant(participant_);

    // As for the constructor, nothing has been added for the two smart pointers \"xml_data_\" and
    // \"xml_type_\", because smart pointers manage auotmatically memory occupancy and deallocation.
}

eprosima::fastrtps::types::TypeKind RegaleObject::data_member_type(eprosima::fastrtps::types::DynamicData_ptr data,
                                                                   eprosima::fastrtps::types::MemberId id) {
    eprosima::fastrtps::types::MemberDescriptor member_descriptor;
    data->get_descriptor(member_descriptor,
                         id);
    return member_descriptor.get_kind();
}
