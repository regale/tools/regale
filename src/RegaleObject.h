#ifndef __REGALEOBJECT_H
#define __REGALEOBJECT_H

#include <fastdds/dds/domain/DomainParticipantFactory.hpp>
#include <fastdds/dds/domain/DomainParticipant.hpp>
#include <fastdds/dds/topic/TypeSupport.hpp>
#include <fastrtps/xmlparser/XMLProfileManager.h>
#include <fastrtps/types/DynamicDataFactory.h>
#include <fastrtps/transport/TransportDescriptorInterface.h>
#include <fastrtps/types/MemberDescriptor.h>
#include <mutex>

class RegaleObject {
protected:
    std::string file_type_;
    static std::once_flag flag_once_;
    void (*ptr_func_)(void*);
    eprosima::fastdds::dds::DomainParticipant* participant_;
    eprosima::fastdds::dds::Topic* topic_;
    eprosima::fastrtps::types::DynamicData_ptr xml_data_; 
    eprosima::fastrtps::types::DynamicType_ptr xml_type_;
    std::shared_ptr<eprosima::fastdds::rtps::TransportDescriptorInterface> transport_;

    eprosima::fastrtps::types::TypeKind data_member_type(eprosima::fastrtps::types::DynamicData_ptr data,
                                                         eprosima::fastrtps::types::MemberId id);

public:
    RegaleObject(char* types_file   ,
                 char* profiles_file,
                 char* transport    ,
                 char* topic        ,
                 void (*ptr_func)(void*) = nullptr);

    virtual ~RegaleObject();
};

#endif
