#include "RegalePublisher.h"

using namespace eprosima::fastdds::dds;

RegalePublisher::RegalePublisher(char* types_file   ,
                                 char* profiles_file,
                                 char* transport    ,
                                 char* topic        ,
                                 void (*ptr_func)(void*)): RegaleObject(types_file   ,
                                                                        profiles_file,
                                                                        transport    ,
                                                                        topic        ,
                                                                        ptr_func) {
    // TODO: manage exceptions for \"publisher_\" and \"writer_\" returned as \"nullptr\".
    publisher_ = participant_->create_publisher(PUBLISHER_QOS_DEFAULT,
                                                nullptr);

    writer_ = publisher_->create_datawriter(topic_                ,
                                            DATAWRITER_QOS_DEFAULT,
                                            &listener_);

    DataWriterQos qos_writer_ = writer_->get_qos();

    ReliabilityQosPolicy reliability_writer_;
    // See https://fast-dds.docs.eprosima.com/en/latest/fastdds/api_reference/dds_pim/core/policy/reliabilityqospolicy.html#classeprosima_1_1fastdds_1_1dds_1_1ReliabilityQosPolicy
    reliability_writer_.kind = RELIABLE_RELIABILITY_QOS;
    // Here the couple represents \"{seconds, nanoseconds}\" (here is equal to 10 microsecond). See:
    // https://fast-dds.docs.eprosima.com/en/latest/fastdds/api_reference/rtps/common/Time_t/fastrtps_Time_t.html#_CPPv4N8eprosima8fastrtps6Time_tE
    reliability_writer_.max_blocking_time = {0, 10000};

    qos_writer_.reliability(reliability_writer_);

    writer_->set_qos(qos_writer_);
}

RegalePublisher::~RegalePublisher() {
    if (writer_ != nullptr)
        publisher_->delete_datawriter(writer_);

    if (publisher_ != nullptr)
        participant_->delete_publisher(publisher_);
}

bool RegalePublisher::publish(uint32_t* values) {
    int i;
    int n_values;

    while (listener_.matched_ < 1)
        std::this_thread::sleep_for(std::chrono::microseconds(10));

    n_values = xml_data_->get_item_count();

    for (i = 0; i < n_values; i++)
        xml_data_->set_uint32_value(values[i],
                                    i);

    writer_->write(&(*xml_data_));

    return true;
}

RegalePublisher::PubListener::PubListener(): matched_(0) {
}

RegalePublisher::PubListener::~PubListener() {
}

void RegalePublisher::PubListener::on_publication_matched(DataWriter*,
                                                          const PublicationMatchedStatus& info) {
    if (info.current_count_change == 1) {
        // If using \"info.total_count\", we will have an only readable value,
        // giving us the total cumulative count of matched writers/readers by
        // topic. See:
        // https://fast-dds.docs.eprosima.com/en/latest/fastdds/api_reference/dds_pim/core/status/matchedstatus.html#_CPPv4N8eprosima7fastdds3dds13MatchedStatusE.
        // Now \"matched_\" is increased of one.
        matched_ = info.current_count;
#ifdef DEBUG
        std::cout << "Publisher matched. Actual number of active matches is: " << info.current_count << std::endl;
#endif
    }
    else if (info.current_count_change == -1) {
        // See previous comment. Now \"matched_\" is reduced by one.
        matched_ = info.current_count;
#ifdef DEBUG
        std::cout << "Publisher unmatched. Actual number of active matches is: " << info.current_count << std::endl;
#endif
    }
    else {
#ifdef DEBUG
        std::cout << info.current_count_change
                << " is not a valid value for PublicationMatchedStatus current count change." << std::endl;
#endif
    }
}
