#ifndef __REGALEPUBLISHER_H
#define __REGALEPUBLISHER_H

#include "RegaleObject.h"
#include <fastdds/dds/publisher/Publisher.hpp>
#include <fastdds/dds/publisher/DataWriter.hpp>
#include <fastdds/dds/publisher/DataWriterListener.hpp>
#include <fastdds/dds/publisher/qos/DataWriterQos.hpp>

class RegalePublisher : public RegaleObject{
protected:
    eprosima::fastdds::dds::Publisher* publisher_;
    eprosima::fastdds::dds::DataWriter* writer_;

    class PubListener : public eprosima::fastdds::dds::DataWriterListener {
    public:

        PubListener();
        ~PubListener() override;
        void on_publication_matched(eprosima::fastdds::dds::DataWriter*,
                                    const eprosima::fastdds::dds::PublicationMatchedStatus& info) override;

        std::atomic_int matched_;

    } listener_;

public:

    RegalePublisher(char* types_file  ,
                    char* pofiles_file,
                    char* transport   ,
                    char* topic       ,
                    void (*ptr_func)(void*) = nullptr);

    ~RegalePublisher();

    bool publish(uint32_t* values);
};

#endif
