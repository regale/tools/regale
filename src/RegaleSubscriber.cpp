#include "RegaleSubscriber.h"
#include <string>

using namespace eprosima::fastdds::dds;

RegaleSubscriber::RegaleSubscriber(char* types_file   ,
                                   char* profiles_file,
                                   char* transport    ,
                                   char* topic        ,
                                   void (*ptr_func)(void*)) : RegaleObject(types_file   ,
                                                                           profiles_file,
                                                                           transport    ,
                                                                           topic        ,
                                                                           ptr_func),
                                                              listener_(*this) {
    // TODO: manage exceptions for \"subscriber_\" and \"reader_\" returned as \"nullptr\".
    subscriber_ = participant_->create_subscriber(SUBSCRIBER_QOS_DEFAULT,
                                                  nullptr);

    reader_ = subscriber_->create_datareader(topic_                ,
                                             DATAREADER_QOS_DEFAULT,
                                             &listener_);

    DataReaderQos qos_reader_ = reader_->get_qos();

    ReliabilityQosPolicy reliability_reader_;
    // See https://fast-dds.docs.eprosima.com/en/latest/fastdds/api_reference/dds_pim/core/policy/reliabilityqospolicy.html#classeprosima_1_1fastdds_1_1dds_1_1ReliabilityQosPolicy
    reliability_reader_.kind = BEST_EFFORT_RELIABILITY_QOS;
    // Here the couple represents \"{seconds, nanoseconds}\" (here is equal to 10 microsecond). See:
    // https://fast-dds.docs.eprosima.com/en/latest/fastdds/api_reference/rtps/common/Time_t/fastrtps_Time_t.html#_CPPv4N8eprosima8fastrtps6Time_tE
    reliability_reader_.max_blocking_time = {0, 10000};

    qos_reader_.reliability(reliability_reader_);

    reader_->set_qos(qos_reader_);
}

RegaleSubscriber::~RegaleSubscriber() {
    if (reader_ != nullptr)
        subscriber_->delete_datareader(reader_);
    
    if (subscriber_ != nullptr)
        participant_->delete_subscriber(subscriber_);
}

RegaleSubscriber::SubListener::SubListener(RegaleSubscriber& s): samples_(0),
                                                                 parent_regale_subscriber_(s) {
}

RegaleSubscriber::SubListener::~SubListener() {
}

void RegaleSubscriber::SubListener::on_subscription_matched(DataReader*,
                                                            const SubscriptionMatchedStatus& info) {
    if (info.current_count_change == 1) {
#ifdef DEBUG
        std::cout << "Subscriber matched." << std::endl;
#endif
    }
    else if (info.current_count_change == -1) {
#ifdef DEBUG
        std::cout << "Subscriber unmatched." << std::endl;
#endif
    }
    else {
#ifdef DEBUG
        std::cout << info.current_count_change
                << " is not a valid value for SubscriptionMatchedStatus current count change" << std::endl;
#endif
    }
}

void RegaleSubscriber::SubListener::on_data_available(DataReader* reader) {
    int i;
    SampleInfo info;
    int n_values;

    if (reader->take_next_sample(&(*(parent_regale_subscriber_.xml_data_)), &info) == ReturnCode_t::RETCODE_OK) {
        if (info.valid_data) {
            n_values = parent_regale_subscriber_.xml_data_->get_item_count();
            uint32_t values[n_values] = {0};

            for (i = 0; i < n_values; i++)
                values[i] = parent_regale_subscriber_.xml_data_->get_uint32_value(i);

            samples_++;

            parent_regale_subscriber_.ptr_func_((void*)values);
            //parent_regale_subscriber_.ptr_func_((void*)&(*parent_regale_subscriber_.xml_data_));

        }
    }
}
