#ifndef __REGALESUBSCRIBER_H
#define __REGALESUBSCRIBER_H

#include "RegaleObject.h"
#include <fastdds/dds/subscriber/Subscriber.hpp>
#include <fastdds/dds/subscriber/DataReader.hpp>
#include <fastdds/dds/subscriber/DataReaderListener.hpp>
#include <fastdds/dds/subscriber/qos/DataReaderQos.hpp>
#include <fastdds/dds/subscriber/SampleInfo.hpp>

class RegaleSubscriber : public RegaleObject {
protected:
    eprosima::fastdds::dds::Subscriber* subscriber_;
    eprosima::fastdds::dds::DataReader* reader_;

    class SubListener : public eprosima::fastdds::dds::DataReaderListener {
    private:
        RegaleSubscriber& parent_regale_subscriber_;
    public:

        SubListener(RegaleSubscriber& s);
        ~SubListener() override;
        void on_subscription_matched(eprosima::fastdds::dds::DataReader*,
                                     const eprosima::fastdds::dds::SubscriptionMatchedStatus& info) override;
        void on_data_available(eprosima::fastdds::dds::DataReader* reader) override;

        std::atomic_int samples_;

    } listener_;

public:

    RegaleSubscriber(char* types_file   ,
                     char* profiles_file,
                     char* transport    ,
                     char* topic        ,
                     void (*ptr_func)(void*) = nullptr);

    ~RegaleSubscriber();
};

#endif
