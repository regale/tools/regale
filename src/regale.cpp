#include "regale.h"
#include <string.h>
#include "RegaleSubscriber.h"
#include "RegalePublisher.h"
#ifdef __cplusplus
extern "C" {
#endif
    RegalePublisher* Regale_create_publisher(char* types_file   ,
                                             char* profiles_file,
                                             char* transport    ,
                                             char* topic) {
        return new RegalePublisher(types_file   ,
                                   profiles_file,
                                   transport    ,
                                   topic);
    }
    RegaleSubscriber* Regale_create_subscriber(char* types_file   ,
                                               char* profiles_file,
                                               char* transport    ,
                                               char* topic        ,
                                               void (*ptr_func)(void*)) {
        return new RegaleSubscriber(types_file   ,
                                    profiles_file,
                                    transport    ,
                                    topic        ,
                                    ptr_func);
    }

    bool Regale_publish(RegalePublisher* p,
                        uint32_t* values) {
        return p->publish(values);
    }

    void Regale_delete(RegaleObject* reg_obj) {
        delete reg_obj;
    }
#ifdef __cplusplus
}
#endif
